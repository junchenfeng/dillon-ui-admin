package com.lw.dillon.ui.fx.view.system.dept;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDeptFeign;
import com.lw.dillon.ui.fx.vo.system.dept.DeptListReqVO;
import com.lw.dillon.ui.fx.vo.system.dept.DeptRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserSimpleRespVO;
import de.saxsys.mvvmfx.SceneLifecycle;
import de.saxsys.mvvmfx.ViewModel;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.*;


/**
 * 部门管理视图模型
 *
 * @author wenli
 * @date 2023/02/15
 */
public class DeptManageViewModel implements ViewModel, SceneLifecycle {
    public final static String OPEN_ALERT = "OPEN_ALERT";

    private SimpleStringProperty title = new SimpleStringProperty("");
    private ObjectProperty<Integer> status = new SimpleObjectProperty<>(null);
    private Map<Long, UserSimpleRespVO> userMap = new HashMap<>();

    private ObservableList<DeptRespVO> deptList = FXCollections.observableArrayList();

    public void rest() {
        title.set("");
        status.set(null);
    }

    public List<DeptRespVO> query() {
        DeptListReqVO reqVO = new DeptListReqVO();
        reqVO.setName(title.get());
        reqVO.setStatus(status.get());
        return Request.connector(SysDeptFeign.class).getDeptList(reqVO).getCheckedData();

    }

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public Integer getStatus() {
        return status.get();
    }

    public ObjectProperty<Integer> statusProperty() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status.set(status);
    }

    public ObservableList<DeptRespVO> getDeptList() {
        return deptList;
    }


    public CommonResult<Boolean> deleteDept(Long id) {
        return Request.connector(SysDeptFeign.class).deleteDept(id);
    }

    public Map<Long, UserSimpleRespVO> getUserMap() {

        return userMap;
    }

    // 场景生命周期
    @Override
    public void onViewAdded() {
        // 当场景中的View添加时触发的方法
    }

    // 场景生命周期
    @Override
    public void onViewRemoved() {
        // 当场景中的View移除时触发的方法
    }


}
