package com.lw.dillon.ui.fx.vo.system.dict.type;

import lombok.Data;

/**
 * 字典类型 Excel 导出响应 VO
 */
@Data
public class DictTypeExcelVO {

    private Long id;

    private String name;

    private String type;

    private Integer status;

}
