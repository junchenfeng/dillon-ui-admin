package com.lw.dillon.ui.fx.vo.system.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserExportReqVO {

    private String username;

    private String mobile;

    private Integer status;

    private LocalDateTime[] createTime;

    private Long deptId;

}
