package com.lw.dillon.ui.fx.view.system.dict.type;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

import java.net.URL;
import java.util.ResourceBundle;

public class DictTypeInfoView implements FxmlView<DictTypeInfoViewModel>, Initializable {

    @InjectViewModel
    private DictTypeInfoViewModel viewModel;


    @FXML
    private RadioButton deactivateRadioBut;

    @FXML
    private ToggleGroup group;

    @FXML
    private TextField nameTextField;

    @FXML
    private RadioButton normalRadioBut;

    @FXML
    private TextArea remarksTextArea;

    @FXML
    private TextField typeTextField;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        nameTextField.textProperty().bindBidirectional(viewModel.nameProperty());
        typeTextField.textProperty().bindBidirectional(viewModel.typeProperty());
        remarksTextArea.textProperty().bindBidirectional(viewModel.remarkProperty());

        normalRadioBut.setUserData(0);
        deactivateRadioBut.setUserData(1);

        initListeners();
    }

    private void initListeners() {

        viewModel.statusProperty().addListener((observable, oldValue, newValue) -> {
            if (ObjectUtil.equal(0, newValue)) {
                normalRadioBut.setSelected(true);
            } else {
                deactivateRadioBut.setSelected(true);
            }
        });
        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (group.getSelectedToggle() != null) {
                viewModel.statusProperty().setValue(NumberUtil.parseInt(group.getSelectedToggle().getUserData().toString()));
            }
        });

    }

}
