package com.lw.dillon.ui.fx.vo.system.notice;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NoticePageReqVO extends PageParam {

    private String title;

    private Integer status;

}
