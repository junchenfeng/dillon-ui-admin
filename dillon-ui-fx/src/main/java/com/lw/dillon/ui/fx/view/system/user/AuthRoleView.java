package com.lw.dillon.ui.fx.view.system.user;

import cn.hutool.core.collection.CollUtil;
import com.lw.dillon.ui.fx.vo.system.role.RoleSimpleRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import eu.hansolo.fx.charts.voronoi.ArraySet;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.StringConverter;
import org.controlsfx.control.CheckComboBox;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * 身份验证角色视图
 *
 * @author wenli
 * @date 2023/05/17
 */
public class AuthRoleView implements FxmlView<AuthRoleViewModel>, Initializable {

    @InjectViewModel
    private AuthRoleViewModel viewModel;

    @FXML
    private TextField nickNameTextField;

    @FXML
    private HBox roleBox;

    @FXML
    private TextField userNameTextField;

    private CheckComboBox<RoleSimpleRespVO> roleCheckComboBox;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        nickNameTextField.textProperty().bind(viewModel.nickNameProperty());
        userNameTextField.textProperty().bind(viewModel.userNameProperty());
        createPostCheckComboBox();

        viewModel.subscribe("initData", (key, payload) -> {
            roleCheckComboBox.getItems().forEach(roleSimpleRespVO -> {

                Set<Long> postIds = viewModel.getUserRoleSet();
                if (CollUtil.isNotEmpty(postIds) && postIds.contains(roleSimpleRespVO.getId())) {
                    roleCheckComboBox.getCheckModel().toggleCheckState(roleSimpleRespVO);
                }
            });
        });
    }

    private void createPostCheckComboBox() {
        roleCheckComboBox = new CheckComboBox<>(viewModel.getRoleSimpleRespVOList());
        roleCheckComboBox.setConverter(new StringConverter<RoleSimpleRespVO>() {
            @Override
            public String toString(RoleSimpleRespVO roleSimpleRespVO) {
                return roleSimpleRespVO.getName();
            }

            @Override
            public RoleSimpleRespVO fromString(String s) {
                return null;
            }
        });
        roleCheckComboBox.focusedProperty().addListener((o, ov, nv) -> {
            if (nv) {
                roleCheckComboBox.show();
            } else {
                roleCheckComboBox.hide();
            }
        });
        roleCheckComboBox.getCheckModel().getCheckedItems().addListener((ListChangeListener<RoleSimpleRespVO>) change -> {
            Set<Long> longSet = new ArraySet<>();
            change.getList().forEach(postSimpleRespVO -> {
                longSet.add(postSimpleRespVO.getId());
            });
            viewModel.getSelRoleSet().clear();
            viewModel.getSelRoleSet().addAll(longSet);

        });
        roleCheckComboBox.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(roleCheckComboBox, Priority.ALWAYS);
        roleBox.getChildren().add(roleCheckComboBox);
    }


}
