package com.lw.dillon.ui.fx.view.system.role;

import cn.hutool.core.util.ObjectUtil;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class RoleInfoView implements FxmlView<RoleInfoViewModel>, Initializable {

    // 注入ViewModel
    @InjectViewModel
    private RoleInfoViewModel viewModel;

    @FXML
    private TextField codeTextField;

    @FXML
    /**
     * remarksTextArea是用于显示备注信息的文本区域组件。
     */
    private TextArea remarksTextArea;


    @FXML
    private TextField roleNameTextField;

    @FXML
    private Spinner<Integer> sortSpinner;

    @FXML
    private ComboBox<Integer> statusComboBox;

    // Initialization
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        // Bind the remarksTextArea to the remarkProperty of the viewModel
        remarksTextArea.textProperty().bindBidirectional(viewModel.remarkProperty());
        // Bind the codeTextField to the roleKeyProperty of the viewModel
        codeTextField.textProperty().bindBidirectional(viewModel.roleKeyProperty());
        // Bind the roleNameTextField to the roleNameProperty of the viewModel
        roleNameTextField.textProperty().bindBidirectional(viewModel.roleNameProperty());
        // Set the converter of the statusComboBox to a StringConverter
        statusComboBox.setConverter(new StringConverter<Integer>() {
            // Convert the statusComboBox to a string
            @Override
            public String toString(Integer object) {
                return ObjectUtil.equal(0, object) ? "开启" : "关闭";
            }

            // Convert the string back to an Integer
            @Override
            public Integer fromString(String string) {
                if (ObjectUtil.equal(string, "开启")) {
                    return 0;
                } else if (ObjectUtil.equal(string, "关闭")) {
                    return 1;
                } else {
                    return null;
                }
            }
        });
        // Add a listener to the statusComboBox to set the statusProperty of the viewModel
        statusComboBox.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            viewModel.statusProperty().set(newValue);
        });
        // Add a listener to the statusProperty of the viewModel to select the statusComboBox
        viewModel.statusProperty().addListener((observableValue, oldValue, newValue) -> {
            statusComboBox.getSelectionModel().select(newValue);
        });
        // Add a listener to the sortSpinner to set the roleSortProperty of the viewModel
        sortSpinner.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
            viewModel.roleSortProperty().set(newValue);
        });
        // Add a listener to the roleSortProperty of the viewModel to set the sortSpinner
        viewModel.roleSortProperty().addListener((observableValue, number, newValue) -> {
            sortSpinner.getValueFactory().setValue(newValue.intValue());
        });
        // Set the value of the sortSpinner to 0
        sortSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));
        sortSpinner.getValueFactory().setValue(0);
    }


}