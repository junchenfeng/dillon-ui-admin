package com.lw.dillon.ui.fx.view.system.operlog;

import atlantafx.base.controls.RingProgressIndicator;
import atlantafx.base.theme.Styles;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.view.control.WFXGenericDialog;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.operatelog.OperateLogRespVO;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewTuple;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.*;
import static atlantafx.base.theme.Tweaks.*;

public class OperLogView implements FxmlView<OperLogViewModel>, Initializable {

    @InjectViewModel
    private OperLogViewModel viewModel;

    @FXML
    private VBox contentPane;

    @FXML
    private Button delBut;

    @FXML
    private TableColumn<OperateLogRespVO, Integer> durationCol;

    @FXML
    private Button emptyBut;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private TableColumn<OperateLogRespVO, Long> idCol;

    @FXML
    private TableColumn<OperateLogRespVO, String> moduleCol;

    @FXML
    private TextField moduleTextField;

    @FXML
    private TableColumn<OperateLogRespVO, String> nameCol;

    @FXML
    private TableColumn<OperateLogRespVO, String> optCol;

    @FXML
    private Button resetBut;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private TableColumn<OperateLogRespVO, LocalDateTime> startTimeCol;

    @FXML
    private TableColumn<OperateLogRespVO, Integer> statusCol;

    @FXML
    private ComboBox<Boolean> successComboBox;

    @FXML
    private TableView<OperateLogRespVO> tableView;

    @FXML
    private TableColumn<OperateLogRespVO, Integer> typeCol;

    @FXML
    private ComboBox<DictDataSimpleRespVO> typeComboBox;

    @FXML
    private TableColumn<OperateLogRespVO, String> userNicknameCol;

    @FXML
    private TextField userNicknameTexField;

    private RingProgressIndicator loading;

    private WFXGenericDialog dialog;


    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bindBidirectional(viewModel.totalProperty());
        viewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        viewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        viewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(viewModel.loadingProperty());

        rootPane.getChildren().add(loading);

        moduleTextField.textProperty().bindBidirectional(viewModel.moduleProperty());
        userNicknameTexField.textProperty().bindBidirectional(viewModel.userNicknameProperty());
        successComboBox.valueProperty().bindBidirectional(viewModel.successProperty());
        typeComboBox.valueProperty().addListener((observableValue, dictDataSimpleRespVO, t1) -> {
            if (t1 != null) {
                viewModel.typeProperty().set(NumberUtil.parseInt(t1.getValue()));

            }
        });
        successComboBox.setConverter(new StringConverter<Boolean>() {
            @Override
            public String toString(Boolean integer) {
                if (integer != null) {
                    return integer ? "成功" : "失败";
                }
                return null;
            }

            @Override
            public Boolean fromString(String s) {
                return null;
            }
        });

        startDatePicker.valueProperty().bindBidirectional(viewModel.startDateProperty());
        endDatePicker.valueProperty().bindBidirectional(viewModel.endDateProperty());
        searchBut.setOnAction(event -> viewModel.updateData());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> viewModel.reset());

        delBut.setOnAction(event -> {

        });
        emptyBut.setOnAction(event -> {
        });

        typeComboBox.setConverter(new StringConverter<DictDataSimpleRespVO>() {
            @Override
            public String toString(DictDataSimpleRespVO dictDataSimpleRespVO) {
                if (dictDataSimpleRespVO != null) {
                    return dictDataSimpleRespVO.getLabel();
                }
                return null;
            }

            @Override
            public DictDataSimpleRespVO fromString(String s) {
                return null;
            }
        });
        typeComboBox.setItems(DictStore.getDictDataList(DictTypeEnum.SYSTEM_OPERATE_TYPE.getDictType()));

        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        moduleCol.setCellValueFactory(new PropertyValueFactory<>("module"));
        userNicknameCol.setCellValueFactory(new PropertyValueFactory<>("userNickname"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("resultCode"));
        startTimeCol.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        durationCol.setCellValueFactory(new PropertyValueFactory<>("duration"));


        statusCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        if (item == 0) {
                            state.setText("成功");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        } else {
                            state.setText("失败");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        typeCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        state.setDisable(false);
                        DictDataSimpleRespVO sysDictData = viewModel.getTypeMap().get(item + "");
                        if (sysDictData != null) {
                            state.setText(sysDictData.getLabel());
                            state.getStyleClass().addAll(sysDictData.getColorType());
                        }

                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });


        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("详细");
                        editBut.setOnAction(event -> showDictDataInfoDialog(getTableRow().getItem()));
                        editBut.setGraphic(FontIcon.of(Feather.EYE));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);

                        HBox box = new HBox(editBut);
                        box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                        setGraphic(box);
                    }
                }
            };
        });


        startTimeCol.setCellFactory(new Callback<TableColumn<OperateLogRespVO, LocalDateTime>, TableCell<OperateLogRespVO, LocalDateTime>>() {
            @Override
            public TableCell<OperateLogRespVO, LocalDateTime> call(TableColumn<OperateLogRespVO, LocalDateTime> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(LocalDateTimeUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                            }
                        }

                    }
                };
            }
        });
        durationCol.setCellFactory(new Callback<TableColumn<OperateLogRespVO, Integer>, TableCell<OperateLogRespVO, Integer>>() {
            @Override
            public TableCell<OperateLogRespVO, Integer> call(TableColumn<OperateLogRespVO, Integer> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(item + "ms");
                            }
                        }

                    }
                };
            }
        });
        tableView.setItems(viewModel.getOperateLogList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn<?, ?> c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }


    }

    public WFXGenericDialog getDialogContent() {
        if (dialog == null) {
            dialog = new WFXGenericDialog();
        }
        return dialog;
    }


    private void showDictDataInfoDialog(OperateLogRespVO sysOperLog) {

        ViewTuple<OperLogInfoView, OperLogInfoViewModel> load = FluentViewLoader.fxmlView(OperLogInfoView.class).load();
        getDialogContent().clearActions();
        load.getViewModel().setOperateLogRespVO(sysOperLog);
        getDialogContent().addActions(Map.entry(new Button("关闭"), event -> dialog.close()));


        getDialogContent().setHeaderIcon(FontIcon.of(Feather.INFO));
        getDialogContent().setHeaderText("操作日志详细");
        getDialogContent().setContent(load.getView());
        getDialogContent().show(rootPane.getScene());
    }


}
