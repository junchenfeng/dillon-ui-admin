package com.lw.dillon.ui.fx.vo.system.menu;

import lombok.Data;


/**
 * 菜单 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class MenuBaseVO {

    private String name;

    private String permission;

    private Integer type;

    private Integer sort;

    private Long parentId;

    private String path;

    private String icon;

    private String component;

    private String componentName;

    private String componentClass;

    private Integer status;

    private Boolean visible;

    private Boolean keepAlive;

    private Boolean alwaysShow;

}
