package com.lw.dillon.ui.fx.vo.system.dept;

import lombok.Data;


@Data
public class DeptListReqVO {

    private String name;

    private Integer status;

}
