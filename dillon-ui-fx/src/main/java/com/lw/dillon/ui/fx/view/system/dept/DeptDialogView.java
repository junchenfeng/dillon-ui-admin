package com.lw.dillon.ui.fx.view.system.dept;

import atlantafx.base.controls.Popover;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserSimpleRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static atlantafx.base.controls.Popover.ArrowLocation.TOP_CENTER;

/**
 * 视图菜单对话框
 *
 * @author wenli
 * @date 2023/02/15
 */
public class DeptDialogView implements FxmlView<DeptDialogViewModel>, Initializable {

    @InjectViewModel
    private DeptDialogViewModel viewModel;
    @FXML
    private TextField emailTextField;

    @FXML
    private ComboBox<UserSimpleRespVO> leaderComboBox;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField parentIdTextField;

    @FXML
    private TextField phoneField;

    @FXML
    private Spinner<Integer> sortSpinner;

    @FXML
    private ComboBox<Integer> statusComboBox;

    private TreeView<DeptSimpleRespVO> deptTreeView;

    private Popover deptTreePopover;


    /**
     * 初始化
     *
     * @param url            url
     * @param resourceBundle 资源包
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        deptTreeView = new TreeView<DeptSimpleRespVO>();
        deptTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                viewModel.setSelectDeptProperty(newValue.getValue());
            }
            if (deptTreePopover != null) {
                deptTreePopover.hide();
            }

        });
        deptTreeView.setCellFactory(new Callback<TreeView<DeptSimpleRespVO>, TreeCell<DeptSimpleRespVO>>() {
            @Override
            public TreeCell<DeptSimpleRespVO> call(TreeView<DeptSimpleRespVO> param) {
                return new TreeCell<>() {
                    @Override
                    protected void updateItem(DeptSimpleRespVO item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                };
            }
        });
        parentIdTextField.setEditable(false);
        viewModel.selectDeptPropertyProperty().addListener((observableValue, deptSimpleRespVO, t1) -> {
            if (t1 != null) {
                parentIdTextField.setText(t1.getName());
            }
        });
        parentIdTextField.setOnMouseClicked(event -> showMenuTreePopover(parentIdTextField));
        nameTextField.textProperty().bindBidirectional(viewModel.nameProperty());
        sortSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));
        sortSpinner.getValueFactory().setValue(0);
        sortSpinner.getValueFactory().valueProperty().bindBidirectional(viewModel.sortProperty());

        emailTextField.textProperty().bindBidirectional(viewModel.emailProperty());
        leaderComboBox.setItems(viewModel.getUserlist());
        leaderComboBox.setConverter(new StringConverter<UserSimpleRespVO>() {
            @Override
            public String toString(UserSimpleRespVO userSimpleRespVO) {
                if (userSimpleRespVO != null) {
                    return userSimpleRespVO.getNickname();
                }
                return null;
            }

            @Override
            public UserSimpleRespVO fromString(String s) {
                return null;
            }
        });
        phoneField.textProperty().bindBidirectional(viewModel.phoneProperty());

        statusComboBox.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer integer) {
                if (integer != null) {
                    return integer == 0 ? "开启" : "关闭";
                }
                return null;
            }

            @Override
            public Integer fromString(String s) {
                return null;
            }
        });
        viewModel.statusProperty().addListener((observableValue, integer, t1) -> {
            if (t1 != null) {
                statusComboBox.getSelectionModel().select(t1);
            }
        });
        statusComboBox.getSelectionModel().selectedItemProperty().addListener((observableValue, integer, t1) -> {
            viewModel.statusProperty().set(t1);
        });

        viewModel.leaderUserIdProperty().addListener((observableValue, aLong, t1) -> {
            if (t1 != null) {
                leaderComboBox.getSelectionModel().select(viewModel.getUserMap().get(t1));
            }
        });
        leaderComboBox.getSelectionModel().selectedItemProperty().addListener((observableValue, userSimpleRespVO, t1) -> viewModel.leaderUserIdProperty().set(t1.getId()));
        viewModel.subscribe("initData", (key, payload) -> initData());

    }

    /**
     * 显示弹出窗口菜单树
     *
     * @param source 源
     */
    private void showMenuTreePopover(Node source) {
        if (deptTreePopover == null) {
            deptTreePopover = new Popover(deptTreeView);
            deptTreePopover.setHeaderAlwaysVisible(false);
            deptTreePopover.setDetachable(false);
            deptTreePopover.setArrowLocation(TOP_CENTER);
        }
        deptTreePopover.setPrefWidth(nameTextField.getWidth() - 40);
//        Bounds bounds = source.localToScreen(source.getBoundsInLocal());
        deptTreePopover.show(source);
    }

    private TreeItem<DeptSimpleRespVO> generateTreeView(List<DeptSimpleRespVO> deptList, Long parentId) {
        TreeItem<DeptSimpleRespVO> rootNode = new TreeItem<>();
        DeptSimpleRespVO respVO = new DeptSimpleRespVO();
        respVO.setId(0L);
        respVO.setName("顶级部门");
        rootNode.setValue(respVO);
        rootNode.setExpanded(true);
        TreeItem<DeptSimpleRespVO> sel = null;
        for (DeptSimpleRespVO dept : deptList) {
            if (dept.getParentId() == parentId) {
                TreeItem<DeptSimpleRespVO> childNode = new TreeItem<>(dept);
                if (dept.getId() == viewModel.parentIdProperty().get()) {
                    sel = childNode;
                }

                childNode.getChildren().addAll(generateTreeView(deptList, dept.getId()).getChildren());
                rootNode.getChildren().add(childNode);
            }
        }
        if (viewModel.parentIdProperty().get() == null) {
            sel = rootNode;
        }
        if (sel != null) {
            deptTreeView.getSelectionModel().select(sel);

        }

        return rootNode;
    }

    private void createDeptTreeData() {

        TreeItem<DeptSimpleRespVO> respVOTreeItem = generateTreeView(viewModel.getDeptList(), 0L);
        deptTreeView.setRoot(respVOTreeItem);

    }

    private void initData() {
        createDeptTreeData();

    }


}
