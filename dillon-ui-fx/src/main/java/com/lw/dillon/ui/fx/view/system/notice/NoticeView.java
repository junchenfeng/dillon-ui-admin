package com.lw.dillon.ui.fx.view.system.notice;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.lw.dillon.ui.fx.domain.SysDictData;
import com.lw.dillon.ui.fx.domain.SysNotice;
import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.view.control.WFXGenericDialog;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeRespVO;
import de.saxsys.mvvmfx.*;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.material2.Material2AL;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;

import static atlantafx.base.theme.Styles.*;
import static atlantafx.base.theme.Tweaks.*;

public class NoticeView implements FxmlView<NoticeViewModel>, Initializable {

    @InjectViewModel
    private NoticeViewModel viewModel;

    @FXML
    private Button addBut;

    @FXML
    private VBox contentPane;

    @FXML
    private TableColumn<NoticeRespVO, LocalDateTime> createTimeCol;

    @FXML
    private Button delBut;

    @FXML
    private Button editBut;

    @FXML
    private TableColumn<NoticeRespVO, Long> idCol;

    @FXML
    private TableColumn<NoticeRespVO, String> optCol;

    @FXML
    private Button resetBut;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private TableColumn<NoticeRespVO, Integer> statusCol;

    @FXML
    private ComboBox<Integer> statusComboBox;

    @FXML
    private TableView<NoticeRespVO> tableView;

    @FXML
    private TableColumn<NoticeRespVO, String> titleCol;

    @FXML
    private TextField titleTextField;

    @FXML
    private TableColumn<NoticeRespVO, Integer> typeCol;

    private RingProgressIndicator loading;

    private ModalPane modalPane;


    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("noticeModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        rootPane.getChildren().add(modalPane);

        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bindBidirectional(viewModel.totalProperty());
        viewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        viewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        viewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(viewModel.loadingProperty());

        rootPane.getChildren().add(loading);

        titleTextField.textProperty().bindBidirectional(viewModel.titleProperty());
        statusComboBox.valueProperty().bindBidirectional(viewModel.statusProperty());
        statusComboBox.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer integer) {
                if (integer != null) {
                    return integer == 0 ? "开启" : "关闭";
                }
                return null;
            }

            @Override
            public Integer fromString(String s) {
                return null;
            }
        });
        searchBut.setOnAction(event -> viewModel.updateData());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> viewModel.reset());
        editBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showDictDataInfoDialog(tableView.getSelectionModel().getSelectedItem().getId());
        });
        delBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showDelDialog(tableView.getSelectionModel().getSelectedItem().getId());
        });


        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        statusCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        Label state = new Label("", new FontIcon(Material2AL.LABEL));
                        if (item == 0) {
                            state.setText("开启");
                            state.getStyleClass().addAll(SUCCESS);
                        } else {
                            state.setText("关闭");
                            state.getStyleClass().addAll(DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        typeCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        Label state = new Label("", new FontIcon(Material2AL.LABEL));
                        DictDataSimpleRespVO dictDataRespVO = DictStore.getDictDataMap(DictTypeEnum.SYSTEM_NOTICE_TYPE.getDictType()).get(item.toString());
                        if (dictDataRespVO != null) {
                            state.setText(dictDataRespVO.getLabel());
                            if (ObjectUtil.equals(dictDataRespVO.getValue(), 1)) {
                                state.getStyleClass().addAll(SUCCESS);
                            } else if (ObjectUtil.equals(dictDataRespVO.getValue(), 2)) {
                                state.getStyleClass().addAll(WARNING);
                            }

                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("修改");
                        editBut.setOnAction(event -> showDictDataInfoDialog(getTableRow().getItem().getId()));
                        editBut.setGraphic(FontIcon.of(Feather.EDIT));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);
                        Button remBut = new Button("删除");
                        remBut.setOnAction(event -> showDelDialog(getTableRow().getItem().getId()));
                        remBut.setGraphic(FontIcon.of(Feather.TRASH));
                        remBut.getStyleClass().addAll(FLAT, ACCENT);


                        HBox box = new HBox(editBut, remBut);
                        box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                        setGraphic(box);
                    }
                }
            };
        });


        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));
        createTimeCol.setCellFactory(new Callback<TableColumn<NoticeRespVO, LocalDateTime>, TableCell<NoticeRespVO, LocalDateTime>>() {
            @Override
            public TableCell<NoticeRespVO, LocalDateTime> call(TableColumn<NoticeRespVO, LocalDateTime> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                            }
                        }

                    }
                };
            }
        });
        tableView.setItems(viewModel.getNoticeList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn<?, ?> c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }


        addBut.setOnAction(event -> showDictDataInfoDialog(null));

    }


    private void showDictDataInfoDialog(Long noticeTypeId) {

        ViewTuple<NoticeInfoView, NoticeInfoViewModel> load = FluentViewLoader.fxmlView(NoticeInfoView.class).load();
        load.getViewModel().initData(noticeTypeId);


        var dialog = new ModalPaneDialog("#noticeModalPane", ObjectUtil.isNotEmpty(noticeTypeId) ? "编辑公告" : "添加公告", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInPlatformThread(() -> load.getViewModel().commitHtmlText()).addSupplierInExecutor(
                            () -> load.getViewModel().save(ObjectUtil.isNotEmpty(noticeTypeId)))
                    .addConsumerInPlatformThread(r -> {
                        if (r) {
                            dialog.close();
                            viewModel.updateData();
                        }
                    }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


    private void showDelDialog(Long id) {

        var dialog = new ModalPaneDialog("#noticeModalPane", "删除公告", new Label("是否确认删除编号为" + id + "的公告吗？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInExecutor(() -> viewModel.deleteNotice(id)).addRunnableInPlatformThread(() -> {
                dialog.close();
                viewModel.updateData();
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


}
