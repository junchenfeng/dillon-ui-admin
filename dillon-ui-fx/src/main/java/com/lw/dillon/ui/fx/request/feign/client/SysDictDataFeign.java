package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataUpdateReqVO;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

/**
 * 数据字典信息
 *
 * @author ruoyi
 */
public interface SysDictDataFeign extends FeignAPI {
    @RequestLine("POST /system/dict-data/create")
//    @Operation(summary = "新增字典数据")
    CommonResult<Long> createDictData(DictDataCreateReqVO reqVO);

    @RequestLine("PUT /system/dict-data/update")
//    @Operation(summary = "修改字典数据")
    CommonResult<Boolean> updateDictData(DictDataUpdateReqVO reqVO);


    @RequestLine("DELETE /system/dict-data/delete?id={id}")
//    @Operation(summary = "删除字典数据")
    CommonResult<Boolean> deleteDictData(@Param("id") Long id);

    @RequestLine("GET /system/dict-data/list-all-simple")
//    @Operation(summary = "获得全部字典数据列表", description = "一般用于管理后台缓存字典数据在本地")
        // 无需添加权限认证，因为前端全局都需要
    CommonResult<List<DictDataSimpleRespVO>> getSimpleDictDataList();

    @RequestLine("GET /system/dict-data/page")
//    @Operation(summary = "/获得字典类型的分页列表")
    CommonResult<PageResult<DictDataRespVO>> getDictTypePage(@QueryMap Map<String, Object> query);

    @RequestLine("GET /system/dict-data/get?id={id}")
//    @Operation(summary = "/查询字典数据详细")
    CommonResult<DictDataRespVO> getDictData(@Param("id") Long id);


}
