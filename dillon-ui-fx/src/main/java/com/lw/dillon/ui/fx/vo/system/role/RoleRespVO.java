package com.lw.dillon.ui.fx.vo.system.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RoleRespVO extends RoleBaseVO {

    private Long id;

    private Integer dataScope;

    private Set<Long> dataScopeDeptIds;

    private Integer status;

    private Integer type;

    private LocalDateTime createTime;

}
