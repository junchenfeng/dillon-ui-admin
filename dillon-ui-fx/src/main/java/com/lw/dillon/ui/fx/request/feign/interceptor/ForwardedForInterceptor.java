package com.lw.dillon.ui.fx.request.feign.interceptor;

import com.lw.dillon.ui.fx.store.AppStore;
import feign.RequestInterceptor;
import feign.RequestTemplate;

public class ForwardedForInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {

        template.header("Authorization", "Bearer " + AppStore.getToken());
        template.header("tenant-id", "1");
    }
}