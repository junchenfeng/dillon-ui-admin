package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantPageReqVO;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantRespVO;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantUpdateReqVO;
import feign.Param;
import feign.RequestLine;

public interface TenantFeign extends FeignAPI {
    @RequestLine("GET /system/tenant/get-id-by-name?name={name}")
    //summary = "使用租户名，获得租户编号", description = "登录界面，根据用户的租户名，获得租户编号")
    public CommonResult<Long> getTenantIdByName(@Param("name") String name);

    @RequestLine("POST /system/tenant/create")
    //summary = "创建租户")
    public CommonResult<Long> createTenant(TenantCreateReqVO createReqVO);

    @RequestLine("PUT /system/tenant/update")
    //summary = "更新租户")
    public CommonResult<Boolean> updateTenant(TenantUpdateReqVO updateReqVO);

    @RequestLine("DELETE /system/tenant/delete")
    //summary = "删除租户")
    public CommonResult<Boolean> deleteTenant(@Param("id") Long id);

    @RequestLine("GET /system/tenant/get")
    //summary = "获得租户")
    public CommonResult<TenantRespVO> getTenant(@Param("id") Long id);

    @RequestLine("GET /system/tenant/page")
    //summary = "获得租户分页")
    public CommonResult<PageResult<TenantRespVO>> getTenantPage(TenantPageReqVO pageVO);


}
