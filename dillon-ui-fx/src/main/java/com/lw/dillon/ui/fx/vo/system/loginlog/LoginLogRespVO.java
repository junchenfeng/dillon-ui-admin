package com.lw.dillon.ui.fx.vo.system.loginlog;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class LoginLogRespVO extends LoginLogBaseVO {

    private Long id;

    private Long userId;

    private Integer userType;

    private LocalDateTime createTime;

}
