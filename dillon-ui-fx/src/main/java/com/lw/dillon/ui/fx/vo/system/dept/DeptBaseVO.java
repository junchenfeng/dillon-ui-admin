package com.lw.dillon.ui.fx.vo.system.dept;

import lombok.Data;

/**
 * 部门 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class DeptBaseVO {

    private String name;

    private Long parentId;

    private Integer sort;

    private Long leaderUserId;

    private String phone;

    private String email;

    private Integer status;

}
