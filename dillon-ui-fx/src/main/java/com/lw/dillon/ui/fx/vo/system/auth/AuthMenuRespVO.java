package com.lw.dillon.ui.fx.vo.system.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthMenuRespVO {

    private Long id;

    private Long parentId;

    private String name;

    private String path;

    private String component;

    private String componentName;

    private String icon;

    private Boolean visible;

    private Boolean keepAlive;

    private Boolean alwaysShow;

    /**
     * 子路由
     */
    private List<AuthMenuRespVO> children;

}
