/* SPDX-License-Identifier: MIT */

package com.lw.dillon.ui.fx.view.control;

import atlantafx.base.controls.Card;
import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.Spacer;
import atlantafx.base.controls.Tile;
import atlantafx.base.layout.ModalBox;
import atlantafx.base.theme.Tweaks;
import com.lw.dillon.ui.fx.view.window.WindowView;
import javafx.beans.NamedArg;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.Nullable;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.util.Objects;

public class ModalPaneDialog extends ModalBox {

    protected final Card content = new Card();
    protected final Tile header = new Tile();

    public ModalPaneDialog(@NamedArg("selector") @Nullable String selector) {
        this(selector, null, FontIcon.of(Feather.INFO), null);
    }

    public ModalPaneDialog(@NamedArg("selector") @Nullable String selector, String tile, Node body) {
        this(selector, tile, FontIcon.of(Feather.INFO), body);
    }

    public ModalPaneDialog(@NamedArg("selector") @Nullable String selector, String tile, Node iconNode, Node body) {
        super(selector);
        createView();
        content.setBody(body);
        setTitle(tile);
        setGraphic(iconNode);

    }

    public void show(Scene scene) {
        var modalPane = (ModalPane) scene.lookup("#" + WindowView.MAIN_MODAL_ID);
        modalPane.show(this);
    }

    protected void createView() {

        content.setHeader(header);
        content.getStyleClass().add(Tweaks.EDGE_TO_EDGE);

        // IMPORTANT: this guarantees client will use correct width and height
        setMinWidth(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMaxHeight(USE_PREF_SIZE);

        AnchorPane.setTopAnchor(content, 0d);
        AnchorPane.setRightAnchor(content, 0d);
        AnchorPane.setBottomAnchor(content, 0d);
        AnchorPane.setLeftAnchor(content, 0d);

        addContent(content);
        getStyleClass().add("modal-dialog");
    }

    public void setTitle(String title) {
        header.setTitle(title);

    }

    public void setDescription(String description) {
        header.setDescription(description);
    }

    public void setGraphic(Node graphicn) {
        header.setGraphic(graphicn);
    }

    public void addFooter(Node node) {
        Objects.requireNonNull(node, "Node cannot be null.");
        content.setFooter(node);
    }


}
