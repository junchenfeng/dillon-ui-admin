package com.lw.dillon.ui.fx.vo.system.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthPermissionInfoRespVO {

    private UserVO user;

    private Set<String> roles;

    private Set<String> permissions;

    private List<MenuVO> menus;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class UserVO {

        private Long id;

        private String nickname;

        private String avatar;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class MenuVO {

        private Long id;

        private Long parentId;

        private String name;

        private String path;

        private String component;

        private String componentName;

        private String icon;

        private Boolean visible;

        private Boolean keepAlive;

        private Boolean alwaysShow;

        /**
         * 子路由
         */
        private List<MenuVO> children;

    }

}
