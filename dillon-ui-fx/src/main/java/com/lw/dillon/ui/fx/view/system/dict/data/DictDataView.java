package com.lw.dillon.ui.fx.view.system.dict.data;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeSimpleRespVO;
import de.saxsys.mvvmfx.*;
import io.datafx.core.concurrent.ProcessChain;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.*;
import static atlantafx.base.theme.Tweaks.*;

public class DictDataView implements FxmlView<DictDataViewModel>, Initializable {

    @InjectViewModel
    private DictDataViewModel viewModel;

    @FXML
    private Button addBut;

    @FXML
    private TableColumn<DictDataRespVO, String> colorTypeCol;

    @FXML
    private VBox contentPane;

    @FXML
    private TableColumn<DictDataRespVO, LocalDateTime> createTimeCol;

    @FXML
    private TableColumn<DictDataRespVO, String> cssClassCol;

    @FXML
    private Button delBut;

    @FXML
    private TableColumn<DictDataRespVO, Integer> dictSortCol;

    @FXML
    private ComboBox<DictTypeSimpleRespVO> dictTypeComboBox;

    @FXML
    private Button editBut;

    @FXML
    private TableColumn<DictDataRespVO, Long> idCol;

    @FXML
    private TableColumn<DictDataRespVO, String> labelCol;

    @FXML
    private TextField labelTextField;

    @FXML
    private TableColumn<DictDataRespVO, String> optCol;

    @FXML
    private TableColumn<DictDataRespVO, String> remarkCol;

    @FXML
    private Button resetBut;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private TableColumn<DictDataRespVO, Integer> statusCol;

    @FXML
    private ComboBox<Integer> statusCombo;

    @FXML
    private TableView<DictDataRespVO> tableView;

    @FXML
    private TableColumn<DictDataRespVO, String> valueCol;

    private ModalPane modalPane;

    private RingProgressIndicator loading;


    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("dict-data-modal-pane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        rootPane.getChildren().add(modalPane);
        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bindBidirectional(viewModel.totalProperty());
        viewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        viewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        viewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(viewModel.loadingProperty());
        rootPane.getChildren().add(loading);

        labelTextField.textProperty().bindBidirectional(viewModel.labelProperty());
        statusCombo.valueProperty().bindBidirectional(viewModel.statusProperty());

        searchBut.setOnAction(event -> viewModel.updateData());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> viewModel.reset());
        editBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showDictDataInfoDialog(tableView.getSelectionModel().getSelectedItem().getId(), tableView.getSelectionModel().getSelectedItem().getDictType());
        });
        delBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showDelDialog(tableView.getSelectionModel().getSelectedItem().getId());
        });
        dictTypeComboBox.setItems(viewModel.getDictTypeList());
        dictTypeComboBox.setConverter(new StringConverter<DictTypeSimpleRespVO>() {
            @Override
            public String toString(DictTypeSimpleRespVO dictTypeSimpleRespVO) {
                if (ObjectUtil.isNotNull(dictTypeSimpleRespVO)) {
                    return dictTypeSimpleRespVO.getName();
                }
                return null;
            }

            @Override
            public DictTypeSimpleRespVO fromString(String s) {
                return null;
            }
        });

        statusCombo.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer integer) {
                if (integer != null) {
                    return integer == 0 ? "开启" : "关闭";
                }
                return null;
            }

            @Override
            public Integer fromString(String s) {
                return null;
            }
        });


        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        labelCol.setCellValueFactory(new PropertyValueFactory<>("label"));
        valueCol.setCellValueFactory(new PropertyValueFactory<>("value"));
        remarkCol.setCellValueFactory(new PropertyValueFactory<>("remark"));
        dictSortCol.setCellValueFactory(new PropertyValueFactory<>("sort"));
        colorTypeCol.setCellValueFactory(new PropertyValueFactory<>("colorType"));
        cssClassCol.setCellValueFactory(new PropertyValueFactory<>("cssClass"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));

        statusCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        if (item == 0) {
                            state.setText("开启");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        } else {
                            state.setText("关闭");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("修改");
                        editBut.setOnAction(event -> showDictDataInfoDialog(getTableRow().getItem().getId(), getTableRow().getItem().getDictType()));
                        editBut.setGraphic(FontIcon.of(Feather.EDIT));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);
                        Button remBut = new Button("删除");
                        remBut.setOnAction(event -> showDelDialog(getTableRow().getItem().getId()));
                        remBut.setGraphic(FontIcon.of(Feather.TRASH));
                        remBut.getStyleClass().addAll(FLAT, ACCENT);


                        HBox box = new HBox(editBut, remBut);
                        box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                        setGraphic(box);
                    }
                }
            };
        });


        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));
        createTimeCol.setCellFactory(new Callback<TableColumn<DictDataRespVO, LocalDateTime>, TableCell<DictDataRespVO, LocalDateTime>>() {
            @Override
            public TableCell<DictDataRespVO, LocalDateTime> call(TableColumn<DictDataRespVO, LocalDateTime> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                        } else {

                            this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));

                        }

                    }
                };
            }
        });
        tableView.setItems(viewModel.getDictDataList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }


        addBut.setOnAction(event -> showDictDataInfoDialog(null, viewModel.getDictType().getType()));


        viewModel.dictTypeProperty().addListener((observableValue, dictTypeSimpleRespVO, t1) -> {
            dictTypeComboBox.getSelectionModel().select(t1);
        });
        dictTypeComboBox.getSelectionModel().selectedItemProperty().addListener((observableValue, dictTypeSimpleRespVO, t1) -> {
            viewModel.dictTypeProperty().set(t1);
        });




    }


    private void showDictDataInfoDialog(Long id, String dictType) {

        ViewTuple<DictDataInfoView, DictDataInfoViewModel> load = FluentViewLoader.fxmlView(DictDataInfoView.class).load();
        load.getViewModel().initData(id, dictType);


        var dialog = new ModalPaneDialog("#dict-data-modal-pane",ObjectUtil.isNotEmpty(id) ? "编辑字典数据" : "添加字典数据", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().save(ObjectUtil.isNotEmpty(id))).addConsumerInPlatformThread(r -> {
                if (r) {
                    dialog.close();
                    viewModel.updateData();
                }
            }).onException(e -> e.printStackTrace()).run();
        });

        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


    private void showDelDialog(Long id) {
        var dialog = new ModalPaneDialog("#dict-data-modal-pane","删除字典数据", new Label("是否确认删除编号为" + id + "的字典数据吗？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInExecutor(() -> viewModel.deleteDictData(id)).addRunnableInPlatformThread(() -> {
                dialog.close();
                viewModel.updateData();
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);

    }


}
