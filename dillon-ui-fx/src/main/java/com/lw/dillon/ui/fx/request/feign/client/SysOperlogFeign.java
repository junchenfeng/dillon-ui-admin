package com.lw.dillon.ui.fx.request.feign.client;

import com.google.gson.JsonObject;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.framework.common.util.collection.CollectionUtils;
import com.lw.dillon.framework.common.util.collection.MapUtils;
import com.lw.dillon.ui.fx.domain.SysOperLog;
import com.lw.dillon.ui.fx.domain.page.TableDataInfo;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.operatelog.OperateLogRespVO;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.lw.dillon.framework.common.pojo.CommonResult.success;

/**
 * 操作日志记录
 *
 * @author ruoyi
 */
public interface SysOperlogFeign extends FeignAPI {
    //    @RequestLine("GET /system/operlog/list")
    @RequestLine("GET /system/operate-log/page")
//    @Operation(summary = "查看操作日志分页列表")
    CommonResult<PageResult<OperateLogRespVO>> pageOperateLog(@QueryMap Map<String, Object> params);


}
