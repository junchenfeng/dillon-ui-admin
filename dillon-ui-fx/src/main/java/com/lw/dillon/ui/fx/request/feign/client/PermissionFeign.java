package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignRoleDataScopeReqVO;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignRoleMenuReqVO;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignUserRoleReqVO;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

public interface PermissionFeign extends FeignAPI {

    //    @Operation(summary = "获得角色拥有的菜单编号")
    @RequestLine("GET /system/permission/list-role-menus?roleId={roleId}")
    public CommonResult<Set<Long>> getRoleMenuList(@Param("roleId") Long roleId);

    @RequestLine("POST /system/permission/assign-role-menu")
//    @Operation(summary = "赋予角色菜单")
    public CommonResult<Boolean> assignRoleMenu(PermissionAssignRoleMenuReqVO reqVO);

    @RequestLine("POST /system/permission/assign-role-data-scope")
//    @Operation(summary = "赋予角色数据权限")
    public CommonResult<Boolean> assignRoleDataScope(PermissionAssignRoleDataScopeReqVO reqVO);

    //    @Operation(summary = "获得管理员拥有的角色编号列表")
    @RequestLine("GET /system/permission/list-user-roles?userId={userId}")
    public CommonResult<Set<Long>> listAdminRoles(@Param("userId") Long userId);

    //    @Operation(summary = "赋予用户角色")
    @RequestLine("POST /system/permission/assign-user-role")
    public CommonResult<Boolean> assignUserRole(PermissionAssignUserRoleReqVO reqVO);

}
