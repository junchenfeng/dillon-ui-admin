package com.lw.dillon.ui.fx.request.feign.client;


import com.google.gson.JsonObject;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.domain.SysNotice;
import com.lw.dillon.ui.fx.domain.page.TableDataInfo;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticePageReqVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeRespVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeUpdateReqVO;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.Map;

import static com.lw.dillon.framework.common.pojo.CommonResult.success;

/**
 * 公告 信息操作处理
 *
 * @author ruoyi
 */
public interface SysNoticeFeign extends FeignAPI {
    @RequestLine("POST /system/notice/create")
//    @Operation(summary = "创建通知公告")
    CommonResult<Long> createNotice(NoticeCreateReqVO reqVO);

    @RequestLine("PUT /system/notice/update")
//    @Operation(summary = "修改通知公告")
    CommonResult<Boolean> updateNotice(NoticeUpdateReqVO reqVO);

    @RequestLine("DELETE /system/notice/delete?id={id}")
//    @Operation(summary = "删除通知公告")
    CommonResult<Boolean> deleteNotice(@Param("id") Long id);

    @RequestLine("GET /system/notice/page")
//    @Operation(summary = "获取通知公告列表")
    CommonResult<PageResult<NoticeRespVO>> getNoticePage(@QueryMap Map<String, Object> queryMap);

    @RequestLine("GET /system/notice/get?id={id}")
//    @Operation(summary = "获得通知公告")
    CommonResult<NoticeRespVO> getNotice(@Param("id") Long id);
}
