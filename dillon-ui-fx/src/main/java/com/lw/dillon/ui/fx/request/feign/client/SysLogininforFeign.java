package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.loginlog.LoginLogRespVO;
import feign.QueryMap;
import feign.RequestLine;

import java.util.Map;

/**
 * 系统访问记录
 *
 * @author ruoyi
 */
public interface SysLogininforFeign extends FeignAPI {
    @RequestLine("GET /system/login-log/page")
    public CommonResult<PageResult<LoginLogRespVO>> getLoginLogPage(@QueryMap Map<String, Object> query);
}
