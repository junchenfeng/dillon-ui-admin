package com.lw.dillon.ui.fx.vo.system.menu;

import lombok.Data;

@Data
public class MenuListReqVO {

    private String name;

    private Integer status;

}
