package com.lw.dillon.ui.fx.vo.system.tenant;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TenantExportReqVO {

    private String name;

    private String contactName;

    private String contactMobile;

    private Integer status;

    private LocalDateTime[] createTime;

}
