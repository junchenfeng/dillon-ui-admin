package com.lw.dillon.ui.fx.vo.system.dict.type;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class DictTypePageReqVO extends PageParam {

    private String name;

    private String type;

    private Integer status;

    private LocalDateTime[] createTime;

}
