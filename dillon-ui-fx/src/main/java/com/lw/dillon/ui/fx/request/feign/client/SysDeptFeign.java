package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.dept.*;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;

/**
 * 部门信息
 *
 * @author ruoyi
 */

public interface SysDeptFeign extends FeignAPI {
    //    @Operation(summary = "创建部门")
    @RequestLine("POST /system/dept/create")
    CommonResult<Long> createDept(DeptCreateReqVO reqVO);

    //    @Operation(summary = "更新部门")
    @RequestLine("PUT /system/dept/update")
    CommonResult<Boolean> updateDept(DeptUpdateReqVO reqVO);

    //    @Operation(summary = "删除部门")
    @RequestLine("DELETE /system/dept/delete?id={id}")
    CommonResult<Boolean> deleteDept(@Param("id") Long id);

    //    @Operation(summary = "获取部门精简信息列表", description = "只包含被开启的部门，主要用于前端的下拉选项")
    @RequestLine("GET /system/dept/list-all-simple")
    CommonResult<List<DeptSimpleRespVO>> getSimpleDeptList();

    //    @Operation(summary = "获取部门列表")
    @RequestLine("GET /system/dept/list")
    CommonResult<List<DeptRespVO>> getDeptList(@QueryMap DeptListReqVO reqVO);

    //    @Operation(summary = "获得部门信息")
    @RequestLine("GET /system/dept/get?id={id}")
    CommonResult<DeptRespVO> getDept(@Param("id") Long id);
}
