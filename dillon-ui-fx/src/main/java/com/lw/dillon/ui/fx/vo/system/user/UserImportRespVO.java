package com.lw.dillon.ui.fx.vo.system.user;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class UserImportRespVO {

    private List<String> createUsernames;

    private List<String> updateUsernames;

    private Map<String, String> failureUsernames;

}
