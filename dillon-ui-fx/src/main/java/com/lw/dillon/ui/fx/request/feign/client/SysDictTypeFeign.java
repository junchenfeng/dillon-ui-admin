package com.lw.dillon.ui.fx.request.feign.client;


import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeUpdateReqVO;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

/**
 * 数据字典信息
 *
 * @author ruoyi
 */
public interface SysDictTypeFeign extends FeignAPI {
    @RequestLine("POST /system/dict-type/create")
//    @Operation(summary = "创建字典类型")
    CommonResult<Long> createDictType(DictTypeCreateReqVO reqVO);

    @RequestLine("PUT /system/dict-type/update")
//    @Operation(summary = "修改字典类型")
    CommonResult<Boolean> updateDictType(DictTypeUpdateReqVO reqVO);

    @RequestLine("DELETE /system/dict-type/delete?id={id}")
//    @Operation(summary = "删除字典类型")
    CommonResult<Boolean> deleteDictType(@Param("id") Long id);

    //    @Operation(summary = "/获得字典类型的分页列表")
    @RequestLine("GET /system/dict-type/page")
    CommonResult<PageResult<DictTypeRespVO>> pageDictTypes(@QueryMap Map<String, Object> query);

    //    @Operation(summary = "/查询字典类型详细")
    @RequestLine("GET /system/dict-type/get?id={id}")
    CommonResult<DictTypeRespVO> getDictType(@Param("id") Long id);

    //    @Operation(summary = "获得全部字典类型列表", description = "包括开启 + 禁用的字典类型，主要用于前端的下拉选项")
    @RequestLine("GET /system/dict-type/list-all-simple")
    // 无需添加权限认证，因为前端全局都需要
    CommonResult<List<DictTypeSimpleRespVO>> getSimpleDictTypeList();


}
