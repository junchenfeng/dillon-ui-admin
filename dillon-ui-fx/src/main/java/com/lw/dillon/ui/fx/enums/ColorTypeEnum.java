package com.lw.dillon.ui.fx.enums;

public enum ColorTypeEnum {
    DEFAULT("默认", "default"),
    PRIMARY("主要", "primary"),
    SUCCESS("成功", "success"),
    INFO("信息", "info"),
    WARNING("警告", "warning"),
    DANGER("危险", "danger");

    private final String label;
    private final String value;

    ColorTypeEnum(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    public static ColorTypeEnum getColorTypeEnumByValue(String value) {
        for (ColorTypeEnum enumValue : ColorTypeEnum.values()) {
            if (enumValue.getValue().equals(value)) {
                return enumValue;
            }
        }
        return null; // 如果没有匹配的枚举值，则返回null或者根据需求进行处理
    }
}