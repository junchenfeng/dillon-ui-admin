package com.lw.dillon.ui.fx.view.system.user;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDeptFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysPostFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysUserFeign;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.post.PostSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserBaseVO;
import com.lw.dillon.ui.fx.vo.system.user.UserCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.user.UserRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserUpdateReqVO;
import de.saxsys.mvvmfx.SceneLifecycle;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;

public class UserInfoViewModel implements ViewModel, SceneLifecycle {


    private StringProperty password = new SimpleStringProperty();
    private ObjectProperty<DeptSimpleRespVO> selectDept = new SimpleObjectProperty<>();
    private ObservableList<PostSimpleRespVO> postSimpleRespVOList = FXCollections.observableArrayList();
    private ObservableList<DeptSimpleRespVO> deptSimpleRespVOList = FXCollections.observableArrayList();
    private ObservableSet<Long> selPosts = FXCollections.observableSet();

    private BooleanProperty editDialog = new SimpleBooleanProperty(false);

    private ObservableSet<Long> postIdSet = FXCollections.observableSet();

    /**
     * 包装器
     */
    private ModelWrapper<UserBaseVO> wrapper = new ModelWrapper<>();

    private Long userId;

    public void initialize() {

    }

    public UserBaseVO getUser() {
        return wrapper.get();
    }

    public void setUser(UserBaseVO user) {

        selPosts.clear();
        if (CollUtil.isNotEmpty(user.getPostIds())) {
            selPosts.addAll(user.getPostIds());
        }
        wrapper.set(user);
        wrapper.reload();
    }

    public StringProperty userNameProperty() {
        return wrapper.field("username", UserBaseVO::getUsername, UserBaseVO::setUsername, "");
    }

    public StringProperty nickNameProperty() {
        return wrapper.field("nickname", UserBaseVO::getNickname, UserBaseVO::setNickname, "");
    }

    public StringProperty emailProperty() {
        return wrapper.field("email", UserBaseVO::getEmail, UserBaseVO::setEmail, "");
    }

    public StringProperty mobileProperty() {
        return wrapper.field("mobile", UserBaseVO::getMobile, UserBaseVO::setMobile, "");
    }

    public LongProperty deptIdProperty() {
        return wrapper.field("deptId", UserBaseVO::getDeptId, UserBaseVO::setDeptId, 0);
    }

    public ObservableSet<Long> getSelPosts() {
        return selPosts;
    }

    public ObservableList<DeptSimpleRespVO> getDeptSimpleRespVOList() {
        return deptSimpleRespVOList;
    }

    public ObservableList<PostSimpleRespVO> getPostSimpleRespVOList() {
        return postSimpleRespVOList;
    }


    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public StringProperty remarkProperty() {
        return wrapper.field("remark", UserBaseVO::getRemark, UserBaseVO::setRemark, "");
    }

    public IntegerProperty sexProperty() {
        return wrapper.field("sex", UserBaseVO::getSex, UserBaseVO::setSex, 0);
    }

    public DeptSimpleRespVO getSelectDept() {
        return selectDept.get();
    }

    public ObjectProperty<DeptSimpleRespVO> selectDeptProperty() {
        return selectDept;
    }

    public void setSelectDept(DeptSimpleRespVO selectDept) {
        this.selectDept.set(selectDept);
    }

    public boolean isEditDialog() {
        return editDialog.get();
    }

    public BooleanProperty editDialogProperty() {
        return editDialog;
    }

    public ObservableSet<Long> getPostIdSet() {
        return postIdSet;
    }

    public void setSelPosts(ObservableSet<Long> selPosts) {
        this.selPosts = selPosts;
    }

    public void initData(Long userId) {
        this.userId = userId;
        editDialog.set(userId != null);
        ProcessChain.create()
                .addSupplierInExecutor(() -> {
                    if (userId == null) {
                        return new UserRespVO();
                    } else {
                        return Request.connector(SysUserFeign.class).getUser(userId).getCheckedData();
                    }
                })
                .addConsumerInPlatformThread(rel -> setUser(rel))
                .addSupplierInExecutor(() -> Request.connector(SysPostFeign.class).getSimplePostList().getCheckedData())
                .addConsumerInPlatformThread(rel -> {
                    postSimpleRespVOList.clear();
                    postSimpleRespVOList.setAll(rel);

                })
                .addSupplierInExecutor(() -> Request.connector(SysDeptFeign.class).getSimpleDeptList().getCheckedData())
                .addConsumerInPlatformThread(rel -> {
                    deptSimpleRespVOList.clear();
                    deptSimpleRespVOList.setAll(rel);
                    publish("initData");
                })
                .onException(e -> e.printStackTrace())
                .run();
    }


    public CommonResult<Long> addUser() {
        wrapper.commit();
        UserCreateReqVO reqVO = new UserCreateReqVO();
        BeanUtil.copyProperties(wrapper.get(), reqVO);
        reqVO.setPassword(password.get());
        reqVO.setDeptId(selectDept.get().getId());
        reqVO.setPostIds(selPosts);
        return Request.connector(SysUserFeign.class).createUser(reqVO);
    }

    public CommonResult<Boolean> updateUser() {
        wrapper.commit();
        UserUpdateReqVO reqVO = new UserUpdateReqVO();
        BeanUtil.copyProperties(wrapper.get(), reqVO);
        reqVO.setId(userId);
        reqVO.setDeptId(selectDept.get().getId());
        reqVO.setPostIds(selPosts);
        return Request.connector(SysUserFeign.class).updateUser(reqVO);
    }

    @Override
    public void onViewAdded() {

    }

    @Override
    public void onViewRemoved() {

    }
}
