package com.lw.dillon.ui.fx.vo.system.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleSimpleRespVO {

    private Long id;

    private String name;

}
