package com.lw.dillon.ui.fx.vo.system.post;

import lombok.Data;

/**
 * 岗位 Excel 导出响应 VO
 */
@Data
public class PostExcelVO {

    private Long id;

    private String code;

    private String name;

    private Integer sort;

    private String status;

}
