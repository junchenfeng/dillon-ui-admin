package com.lw.dillon.ui.fx.vo.system.role;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RoleExportReqVO {

    private String name;

    private String code;

    private Integer status;

    private LocalDateTime[] createTime;

}
