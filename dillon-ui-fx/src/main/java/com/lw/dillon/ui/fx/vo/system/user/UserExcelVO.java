package com.lw.dillon.ui.fx.vo.system.user;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 用户 Excel 导出 VO
 */
@Data
public class UserExcelVO {

    private Long id;

    private String username;

    private String nickname;

    private String email;

    private String mobile;

    private Integer sex;

    private Integer status;

    private String loginIp;

    private LocalDateTime loginDate;

    private String deptName;

    private String deptLeaderNickname;

}
