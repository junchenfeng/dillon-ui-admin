package com.lw.dillon.ui.fx.view.system.post;

import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class PostInfoView implements FxmlView<PostInfoViewModel>, Initializable {

    @InjectViewModel
    private PostInfoViewModel viewModel;

    @FXML
    private TextField codeTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextArea remarksTextArea;

    @FXML
    private Spinner<Integer> sortSpinner;

    @FXML
    private ComboBox<Integer> statusComboBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        remarksTextArea.textProperty().bindBidirectional(viewModel.remarkProperty());
        codeTextField.textProperty().bindBidirectional(viewModel.postCodeProperty());
        nameTextField.textProperty().bindBidirectional(viewModel.postNameProperty());
        sortSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));
        sortSpinner.getValueFactory().setValue(0);
        sortSpinner.getValueFactory().valueProperty().bindBidirectional(viewModel.postSortProperty());

        statusComboBox.valueProperty().addListener((observableValue, integer, t1) -> viewModel.statusProperty().set(t1));
        viewModel.statusProperty().addListener((observableValue, integer, t1) -> {
            if (t1 != null) {
                statusComboBox.getSelectionModel().select(t1);
            }
        });
        statusComboBox.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer integer) {
                if (integer != null) {
                    return integer == 0 ? "开启" : "关闭";
                }
                return null;
            }

            @Override
            public Integer fromString(String s) {
                return null;
            }
        });
    }


}
