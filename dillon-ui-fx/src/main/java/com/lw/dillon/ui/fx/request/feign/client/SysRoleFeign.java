package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.role.*;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

public interface SysRoleFeign extends FeignAPI {


    //    @Operation(summary = "创建角色")
    @RequestLine("POST /system/role/create")
    public CommonResult<Long> createRole(RoleCreateReqVO reqVO);

    //    @Operation(summary = "修改角色")
    @RequestLine("PUT /system/role/update")
    public CommonResult<Boolean> updateRole(RoleUpdateReqVO reqVO);

    //    @Operation(summary = "修改角色状态")
    @RequestLine("PUT /system/role/update-status")
    public CommonResult<Boolean> updateRoleStatus(RoleUpdateStatusReqVO reqVO);

    //    @Operation(summary = "删除角色")
    @RequestLine("DELETE /system/role/delete?id={id}")
    public CommonResult<Boolean> deleteRole(@Param("id") Long id);

    //    @Operation(summary = "获得角色信息")
    @RequestLine("GET /system/role/get?id={id}")
    public CommonResult<RoleRespVO> getRole(@Param("id") Long id);

    //    @Operation(summary = "获得角色分页")
    @RequestLine("GET /system/role/page")
    public CommonResult<PageResult<RoleDO>> getRolePage(@QueryMap Map<String,Object> querMap);

    //    @Operation(summary = "获取角色精简信息列表", description = "只包含被开启的角色，主要用于前端的下拉选项")
    @RequestLine("GET /system/role/list-all-simple")
    public CommonResult<List<RoleSimpleRespVO>> getSimpleRoleList();

    @RequestLine("GET /system/role/export")
    public void export(@QueryMap RoleExportReqVO reqVO);
}
