package com.lw.dillon.ui.fx.view.system.dict.type;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.view.system.dict.data.DictDataView;
import com.lw.dillon.ui.fx.view.system.dict.data.DictDataViewModel;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeRespVO;
import de.saxsys.mvvmfx.*;
import io.datafx.core.concurrent.ProcessChain;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;

import static atlantafx.base.theme.Styles.*;
import static atlantafx.base.theme.Tweaks.*;

public class DictTypeView implements FxmlView<DictTypeViewModel>, Initializable {

    @InjectViewModel
    private DictTypeViewModel dictTypeViewModel;

    @FXML
    private Button addBut;

    @FXML
    private VBox contentPane;

    @FXML
    private TableColumn<DictTypeRespVO, LocalDateTime> createTimeCol;

    @FXML
    private Button delBut;

    @FXML
    private Button editBut;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private TableColumn<DictTypeRespVO, Long> idCol;

    @FXML
    private TableColumn<DictTypeRespVO, String> nameCol;

    @FXML
    private TextField nameTextField;

    @FXML
    private TableColumn<DictTypeRespVO, String> optCol;

    @FXML
    private TableColumn<DictTypeRespVO, String> remarkCol;

    @FXML
    private Button resetBut;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private TableColumn<DictTypeRespVO, Integer> statusCol;

    @FXML
    private ComboBox<Integer> statusComboBox;

    @FXML
    private TableView<DictTypeRespVO> tableView;

    @FXML
    private TableColumn<DictTypeRespVO, String> typeCol;

    @FXML
    private TextField typeTextField;


    private RingProgressIndicator loading;

    private ModalPane modalPane;

    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("dictTypeModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });

        rootPane.getChildren().add(modalPane);


        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bindBidirectional(dictTypeViewModel.totalProperty());
        dictTypeViewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        dictTypeViewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        dictTypeViewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            dictTypeViewModel.queryDictDataList();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            dictTypeViewModel.queryDictDataList();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(dictTypeViewModel.lodingProperty());

        rootPane.getChildren().add(loading);

        statusComboBox.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer integer) {
                if (ObjectUtil.isNotNull(integer)) {
                    return integer == 0 ? "开启" : "关闭";
                }
                return null;
            }

            @Override
            public Integer fromString(String s) {
                return null;
            }
        });

        nameTextField.textProperty().bindBidirectional(dictTypeViewModel.nameProperty());
        typeTextField.textProperty().bindBidirectional(dictTypeViewModel.typeProperty());
        statusComboBox.valueProperty().bindBidirectional(dictTypeViewModel.statusProperty());
        startDatePicker.valueProperty().bindBidirectional(dictTypeViewModel.startDateProperty());
        endDatePicker.valueProperty().bindBidirectional(dictTypeViewModel.endDateProperty());

        searchBut.setOnAction(event -> dictTypeViewModel.queryDictDataList());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> dictTypeViewModel.reset());
        editBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showDictDataInfoDialog(tableView.getSelectionModel().getSelectedItem().getId());
        });
        delBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showDelDialog(tableView.getSelectionModel().getSelectedItem().getId());
        });


        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        remarkCol.setCellValueFactory(new PropertyValueFactory<>("remark"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));


        statusCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        if (item == 0) {
                            state.setText("开启");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        } else {
                            state.setText("关闭");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        typeCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        Button editBut = new Button(item);
                        editBut.setOnAction(event -> {
                            ViewTuple<DictDataView, DictDataViewModel> load = FluentViewLoader.fxmlView(DictDataView.class).load();
                            load.getViewModel().initData(getTableRow().getItem().getType());
                            MvvmFX.getNotificationCenter().publish("addTab", "字典数据", "", load.getView());
                        });
                        editBut.getStyleClass().addAll(FLAT, ACCENT);
                        HBox box = new HBox(editBut);
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("修改");
                        editBut.setOnAction(event -> showDictDataInfoDialog(getTableRow().getItem().getId()));
                        editBut.setGraphic(FontIcon.of(Feather.EDIT));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);
                        Button dataBut = new Button("数据");
                        dataBut.setOnAction(event -> {
                            ViewTuple<DictDataView, DictDataViewModel> load = FluentViewLoader.fxmlView(DictDataView.class).load();
                            load.getViewModel().initData(getTableRow().getItem().getType());
                            MvvmFX.getNotificationCenter().publish("addTab", "字典数据", "", load.getView());
                        });
                        dataBut.setGraphic(FontIcon.of(Feather.EDIT));
                        dataBut.getStyleClass().addAll(FLAT, ACCENT);
                        Button remBut = new Button("删除");
                        remBut.setOnAction(event -> showDelDialog(getTableRow().getItem().getId()));
                        remBut.setGraphic(FontIcon.of(Feather.TRASH));
                        remBut.getStyleClass().addAll(FLAT, ACCENT);
                        HBox box = new HBox(editBut, dataBut, remBut);
                        box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                        setGraphic(box);
                    }
                }
            };
        });


        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));
        createTimeCol.setCellFactory(tableView -> new TableCell<>() {
            @Override
            protected void updateItem(LocalDateTime localDateTime, boolean b) {
                super.updateItem(localDateTime, b);
                if (b || localDateTime == null) {
                    setText(null);
                } else {
                    this.setText(DateUtil.format(localDateTime, "yyyy-MM-dd HH:mm:ss"));
                }
            }
        });
        tableView.setItems(dictTypeViewModel.getDictTypeList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }
        addBut.setOnAction(event -> showDictDataInfoDialog(null));

    }



    private void showDictDataInfoDialog(Long dictTypeId) {

        ViewTuple<DictTypeInfoView, DictTypeInfoViewModel> load = FluentViewLoader.fxmlView(DictTypeInfoView.class).load();
        load.getViewModel().initData(dictTypeId);

        var dialog = new ModalPaneDialog("#dictTypeModalPane",ObjectUtil.isNotEmpty(dictTypeId) ? "编辑字典类型" : "添加字典类型", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().save(ObjectUtil.isNotEmpty(dictTypeId))).addConsumerInPlatformThread(r -> {
                if (r) {
                    dialog.close();
                    dictTypeViewModel.queryDictDataList();
                }
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


    private void showDelDialog(Long id) {

        var dialog = new ModalPaneDialog("#dictTypeModalPane","删除字典类型", new Label("是否确认删除编号为" + id + "的字典类型吗？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInExecutor(() -> dictTypeViewModel.deleteDictType(id)).addRunnableInPlatformThread(() -> {
                dialog.close();
                dictTypeViewModel.queryDictDataList();
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);



    }


}
