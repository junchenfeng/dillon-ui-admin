package com.lw.dillon.ui.fx.vo.system.user;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserPageReqVO extends PageParam {

    private String username;

    private String mobile;

    private Integer status;

    private LocalDateTime[] createTime;

    private Long deptId;

}
