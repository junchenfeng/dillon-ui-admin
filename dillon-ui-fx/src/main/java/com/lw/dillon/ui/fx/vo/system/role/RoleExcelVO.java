package com.lw.dillon.ui.fx.vo.system.role;

import lombok.Data;

/**
 * 角色 Excel 导出响应 VO
 */
@Data
public class RoleExcelVO {

    private Long id;

    private String name;

    private String code;

    private Integer sort;

    private Integer dataScope;

    private String status;

}
