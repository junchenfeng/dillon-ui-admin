package com.lw.dillon.ui.fx.view.system.dict.data;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.enums.ColorTypeEnum;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class DictDataInfoView implements FxmlView<DictDataInfoViewModel>, Initializable {

    @InjectViewModel
    private DictDataInfoViewModel viewModel;


    @FXML
    private ComboBox<ColorTypeEnum> colorTypeComboBox;

    @FXML
    private TextField cssClassTextField;

    @FXML
    private RadioButton deactivateRadioBut;

    @FXML
    private ToggleGroup group;

    @FXML
    private TextField labelTextField;

    @FXML
    private RadioButton normalRadioBut;

    @FXML
    private TextArea remarksTextArea;

    @FXML
    private Spinner<Integer> sortSpinner;

    @FXML
    private TextField typeTtexField;

    @FXML
    private TextField valueTextField;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        colorTypeComboBox.setConverter(new StringConverter<ColorTypeEnum>() {
            @Override
            public String toString(ColorTypeEnum colorTypeEnum) {
                if (colorTypeEnum != null) {
                    return colorTypeEnum.getLabel() + "(" + colorTypeEnum.getValue() + ")";
                }
                return null;
            }

            @Override
            public ColorTypeEnum fromString(String s) {
                return null;
            }
        });
        colorTypeComboBox.setItems(viewModel.getColorTypeEnumList());
        cssClassTextField.textProperty().bindBidirectional(viewModel.cssClassProperty());
        labelTextField.textProperty().bindBidirectional(viewModel.labelProperty());
        typeTtexField.textProperty().bindBidirectional(viewModel.typeProperty());
        valueTextField.textProperty().bindBidirectional(viewModel.valueProperty());
        remarksTextArea.textProperty().bindBidirectional(viewModel.remarkProperty());
        sortSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));

        normalRadioBut.setUserData(0);
        deactivateRadioBut.setUserData(1);

        initListeners();
    }

    private void initListeners() {
        colorTypeComboBox.valueProperty().addListener((observable, oldValue, newValue) -> viewModel.colorTypeProperty().setValue(newValue.getValue()));
        viewModel.colorTypeProperty().addListener((observable, oldValue, newValue) -> {
            colorTypeComboBox.getSelectionModel().select(ColorTypeEnum.getColorTypeEnumByValue(newValue));
        });
        viewModel.sortProperty().addListener((observable, oldValue, newValue) -> {
            sortSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, newValue.intValue()));
        });
        sortSpinner.valueProperty().addListener((observable, oldValue, newValue) -> viewModel.sortProperty().setValue(newValue));

        viewModel.statusProperty().addListener((observable, oldValue, newValue) -> {
            if (ObjectUtil.equal(0, newValue)) {
                normalRadioBut.setSelected(true);
            } else {
                deactivateRadioBut.setSelected(true);
            }
        });
        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (group.getSelectedToggle() != null) {
                viewModel.statusProperty().setValue(NumberUtil.parseInt(group.getSelectedToggle().getUserData().toString()));
            }
        });


    }


}
