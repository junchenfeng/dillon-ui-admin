package com.lw.dillon.ui.fx.view.system.operlog;

import cn.hutool.core.date.DateUtil;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class OperLogInfoView implements FxmlView<OperLogInfoViewModel>, Initializable {

    @InjectViewModel
    private OperLogInfoViewModel viewModel;

    @FXML
    private TextArea content;

    @FXML
    private TextField duration;

    @FXML
    private TextField exts;

    @FXML
    private TextField id;

    @FXML
    private TextField javaMethod;

    @FXML
    private TextArea javaMethodArgs;

    @FXML
    private TextField module;

    @FXML
    private TextField name;

    @FXML
    private TextField requestMethod;

    @FXML
    private TextField resultCode;

    @FXML
    private TextArea resultData;

    @FXML
    private TextField resultMsg;

    @FXML
    private TextField startTime;

    @FXML
    private TextField traceId;

    @FXML
    private TextField userAgent;

    @FXML
    private TextField userId;

    @FXML
    private TextField userIp;

    @FXML
    private TextField userNickname;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        content.textProperty().bind(viewModel.contentCodeProperty());
        duration.textProperty().bind(Bindings.createStringBinding(
                () -> viewModel.durationProperty().get() + "ms",
                viewModel.durationProperty()));
        exts.textProperty().bind(Bindings.createStringBinding(
                () -> viewModel.extsProperty().get() + "",
                viewModel.extsProperty()));

        id.textProperty().bind(Bindings.createStringBinding(
                () -> viewModel.idProperty().get() + "",
                viewModel.idProperty()));

        javaMethod.textProperty().bind(viewModel.javaMethodProperty());


        javaMethodArgs.textProperty().bind(viewModel.javaMethodArgsProperty());

        module.textProperty().bind(viewModel.moduleProperty());

        name.textProperty().bind(viewModel.nameProperty());
        requestMethod.textProperty().bind(viewModel.requestMethodUrlProperty());


        resultCode.textProperty().bind(Bindings.createStringBinding(
                () -> viewModel.resultCodeProperty().get() == 0 ? "正常" : "失败",
                viewModel.resultCodeProperty()));


        resultData.textProperty().bind(viewModel.resultDataProperty());


        resultMsg.textProperty().bind(viewModel.resultMsgProperty());


        startTime.textProperty().bind(Bindings.createStringBinding(
                () -> DateUtil.format(viewModel.startTimeProperty().get(), "yyyy-MM-dd HH:mm:ss"),
                viewModel.startTimeProperty()));

        traceId.textProperty().bind(viewModel.traceIdProperty());


        userAgent.textProperty().bind(viewModel.userAgentProperty());


        userId.textProperty().bind(Bindings.createStringBinding(
                () -> viewModel.userIdProperty().get() + "",
                viewModel.userIdProperty()));

        userIp.textProperty().bind(viewModel.userIpProperty());


        userNickname.textProperty().bind(viewModel.userNicknameProperty());


    }

    private void initListeners() {


    }


}
