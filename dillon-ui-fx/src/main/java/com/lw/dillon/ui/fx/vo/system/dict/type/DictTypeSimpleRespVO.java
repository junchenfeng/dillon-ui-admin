package com.lw.dillon.ui.fx.vo.system.dict.type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DictTypeSimpleRespVO {

    private Long id;

    private String name;

    private String type;

}
