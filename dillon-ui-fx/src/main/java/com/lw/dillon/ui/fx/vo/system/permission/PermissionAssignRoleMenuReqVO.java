package com.lw.dillon.ui.fx.vo.system.permission;

import lombok.Data;

import java.util.Collections;
import java.util.Set;

@Data
public class PermissionAssignRoleMenuReqVO {

    private Long roleId;

    private Set<Long> menuIds = Collections.emptySet(); // 兜底

}
