package com.lw.dillon.ui.fx.vo.system.role;

import com.lw.dillon.framework.common.enums.CommonStatusEnum;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * 角色 DO
 *
 * @author ruoyi
 */
@Data
public class RoleDO {

    /**
     * 角色ID
     */
    private Long id;
    /**
     * 角色名称
     */
    private String name;
    /**
     * 角色标识
     * <p>
     * 枚举
     */
    private String code;
    /**
     * 角色排序
     */
    private Integer sort;
    /**
     * 角色状态
     * <p>
     * 枚举 {@link CommonStatusEnum}
     */
    private Integer status;
    /**
     * 角色类型
     */
    private Integer type;
    /**
     * 备注
     */
    private String remark;

    /**
     * 数据范围
     */
    private Integer dataScope;
    /**
     * 数据范围(指定部门数组)
     * <p>
     */
    private Set<Long> dataScopeDeptIds;

    private LocalDateTime createTime;

}
