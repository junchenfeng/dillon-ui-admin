/* SPDX-License-Identifier: MIT */

package com.lw.dillon.ui.fx.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
public @interface Listener {
}